Atmel AVR C library: TWI master and slave driver
================================================

About
=====

<https://gitlab.com/marsin/myAVR-MK3_c-driver_libtwi>

This project implements TWI master and slave driver functions.
It has some test functions, selected by the compiler directive `TEST`,
declared in the Makefile or by the make command.
The Makefile is configured for installing the TWI functions
as library on the local system with an easy command.
The test functions are not required for compiling and installing the TWI library.
For installing the library for multiple Instruction Set Architectures (ISA)
edit and use the script `install-libs.sh` (edit the Makefile as well).

For implementing this project, a myAVR-MK3 development board was/is used,
but the functions and library is designed for general usage on multiple AVR ISA.
For the testing system, a myAVR-MK2 development board is used.

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


License
-------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


Requirements
------------

### General

* avr-gcc
* avrdude


### Libraries

The MK3 library is required for the test functions,
but not for compiling and installing this TWI library.

* <https://gitlab.com/marsin/myAVR-MK3_c-driver_libmk3>


Counterpart Project
-------------------

The `myAVR-MK2_c-test_libtwi` project is the counterpart to this project,
using a myAVR MK2 development board with an ATmega8 controller.
It serves as communication partner for function and compatibly testing.

It is not required for installing and using this TWI library.
It uses this TWI library and shows how to use this library.

* <https://gitlab.com/marsin/myAVR-MK2_c-test_libtwi>


Documentation
-------------

Creating the Doxygen reference:

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Usage
-----

Edit the Makefile!

* `make all`        Make software.
* `make clean`      Clean out built project files.
* `make program`    Download the hex file to the device, using avrdude.
* `make install`    Install drivers as library and headers on system.
* `make uninstall`  Uninstall driver library and headers from system.


### Installing the library

Use `install-libs.sh` for compiling and installing the library for multiple ISA.
A TEST directive is not required.
Edit the script and the Makefile for other Instruction Set Architectures (ISA).

* The library becomes installed under
  - `/usr/local/avr/lib/avr4/mars/libtwi.a`
  - `/usr/local/avr/lib/avr5/mars/libtwi.a`
  - `/usr/local/avr/lib/avr6/mars/libtwi.a`
* The headers become installed under
  - `/usr/local/avr/include/mars/twi/master.h`
  - `/usr/local/avr/include/mars/twi/slave.h`
* Include the headers with
  - `#include <mars/twi/master.h>`
  - `#include <mars/twi/slave.h>`
* Link the library with (depending on your ISA)
  - `-ltwi -L/usr/local/avr/lib/avr4/mars`
  - `-ltwi -L/usr/local/avr/lib/avr5/mars`
  - `-ltwi -L/usr/local/avr/lib/avr6/mars`


### Programming the MC (MK3 board)

Select a test function with the compiler directive TEST,
defined in the Makefile or by the make command.

	$ make clean && make TEST=1 && make install


Library Functions
=================

TWI Master Driver Functions
---------------------------

* src/master.h
* src/master.c


TWI Slave Driver Functions
--------------------------

* src/slave.h
* src/slave.c


Test Functions
==============

Not all test functions are implemented yet!

* src/test.h
* src/test.c


Test System
-----------

![Image: TWI test circuit](./img/photo_test_circuit.jpg "TWI test circuit with myAVR MK2 & MK3 board and port expander IC")

* myAVR MK2 board:
  - en: <http://shop.myavr.com/?sp=article.sp.php&artID=40>
  - de: <http://shop.myavr.de/?sp=article.sp.php&artID=40>
* myAVR MK3 board:
  - en: <http://shop.myavr.com/?sp=article.sp.php&artID=100063>
  - de: <http://shop.myavr.de/?sp=article.sp.php&artID=100063>

For the breadboard configuration and the port extension IC
have a look below to the example functions and circuits.


Example Functions
=================

This functions are for the PCF8574AP port extension IC.

- Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
- The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
- The maximal operation frequency is 100kHz              (_Page 16_)
- The I/Os should be HIGH before being used as inputs    (_Page 12_)
- This IC has a 100uA current driver.
  Because of this, the button SW0 requires no pre resistor or pull-up resistor.
  - <https://www.mikrocontroller.net/articles/Port-Expander_PCF8574>


MK3 Board Ports | PCF8574AP
--------------- | ---------
+5V             | Vdd
GND             | Vss
PORT.D0         | SCL
PORT.D1         | SDA


![Image: TWI breadboard](./img/photo_twi.jpg "Breadboard with TWI resistor configuration and port expander IC")


Function (Example 1: blinken light)
-----------------------------------

The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
When the connections works, the MC sends data bytes
which becomes written on the port output of the port extension IC.
Is a data bit low, the drain get connected to GND and the LED lights up.


### The Circuit

	                         DIP16
	                     ---------
	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
	GND -------------A1-|02     15|-SDA--------'------ SDA
	GND -------------A2-|03     14|-SCL--------.------ SCL
	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
	                -P1-|05  C  12|-P7-
	                -P2-|06     11|-P6-
	                -P3-|07     10|-P5-
	GND ------------Vss-|08     09|-P4-
	                     ---------
	                     PCF8574AP


Function (Example 2: button and light)
--------------------------------------

The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
When the connections works, the MC sends 0xFF for init the PORTs first.
The I/Os should be HIGH before being used as inputs.
The TWI-IC becomes read, the state of P7 (button SW0) checked and P0 (LED)
depending on the state of P7 written.


### Circuit

	                         DIP16
	                     ---------
	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
	GND -------------A1-|02     15|-SDA--------'------ SDA
	GND -------------A2-|03     14|-SCL--------.------ SCL
	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
	                -P1-|05  C  12|-P7-------SW0------ GND
	                -P2-|06     11|-P6-
	                -P3-|07     10|-P5-
	GND ------------Vss-|08     09|-P4-
	                     ---------
	                     PCF8574AP

