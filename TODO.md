TODO
====

Next
----

* Reconfigure `Makefile` for installing the libs in subfolder 'mars'
* Implementing shell script for automated compiling and installing libraries for all supported architectures.
  - Every run requires different MCU, F_CPU, ISA.
* Implementing testing rest of test functions.

* Implementing a CONTINUE mode
  - Works like RESET mode, without a buffer address data byte.
  - Doesn't reset to buffer position 0 on a new data transmission - continues on the actual position.

