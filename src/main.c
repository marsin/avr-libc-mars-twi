/******************************************************************************
 Atmel - C driver
   - driver development in C for Atmel microcontroller,
     using the MyAVR MK3 development board
 Copyright (c) 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Main program.
 *
 * @file      main.c
 * @date      2017
 * @author    Martin Singer
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#include "test.h"


#ifndef TEST
  #define TEST 0
#endif


/** Main function.
 *
 * Choose one of the test functions by editing the variable TEST in the Makefile,
 * or directly with the make command.
 *
 * e.g.
 *
 *	make clean && make TEST=1
 */
int main(void)
{
#if TEST == 1
	test_twi_master_transmitter_port_extension();
#elif TEST == 2
	test_twi_master_transmitter_reset_mode();
#elif TEST == 3
	test_twi_master_transmitter_address_mode();
#elif TEST == 4
	test_twi_master_receiver_port_extension();
#elif TEST == 5
	test_twi_master_receiver_reset_mode();
#elif TEST == 6
	test_twi_master_receiver_address_mode();
#elif TEST == 7
	test_twi_slave_transmitter_port_extension();
#elif TEST == 8
	test_twi_slave_transmitter_reset_mode();
#elif TEST == 9
	test_twi_slave_transmitter_address_mode();
#elif TEST == 10
	test_twi_slave_receiver_port_extension();
#elif TEST == 11
	test_twi_slave_receiver_reset_mode();
#elif TEST == 12
	test_twi_slave_receiver_address_mode();
#elif TEST == 254
	example_twi_master_blink();
#elif TEST == 255
	example_twi_master_button();
#else
  #error No valid TEST function selected!
#endif // TEST

	while (!0) {
		;
	}

	return 0;
}

