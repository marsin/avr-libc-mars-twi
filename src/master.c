/******************************************************************************
 Atmel - C driver - TWI master
   - TWI (I2C) master driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** TWI (I2C) master driver.
 *
 * @file      master.c
 * @see       master.h
 * @author    Peter Fleury,
 *            Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 *
 *
 * About
 * =====
 *
 * - This driver is tested with PCF8574AP (PIC16) IC.
 * - Bases on the __twimaster library__ from __Peter Fleury__
 *   <http://homepage.hispeed.ch/peterfleury/avr-software.html#libs>
 *   <pfleury@gmx.ch>.
 *
 * Example
 * =======
 *
 *	uint8_t ret = 0x00;
 *	
 *	ret = twi_master_start(0x70 | TWI_MASTER_WRITE);
 *	if (ret == 0) {
 *		ret = twi_master_write(0xFF);
 *		twi_master_stop();
 *		if (ret == 0) {
 *			; // ok
 *		} else {
 *			; // fail
 *		}
 *	} else {
 *		twi_master_stop();
 *	}
 */


#include <util/twi.h>
#include "master.h"


/** Initialize the TWI master interface.
 *
 * The Registers
 * =============
 *
 * - TWI Status Register - TWSR
 *   - Bits 7..3 - TWS:  TWI Status         (r)
 *   - Bit  2    - reserved                 (r)
 *   - Bits 1..0 - TWSP: TWI Prescaler Bits (r/w)
 *
 *     TWPS1 | TWPS0 | Prescaler Value
 *     ----- | ----- | ---------------
 *     0     | 0     | 1     = 4 exp 0
 *     0     | 1     | 4     = 4 exp 1
 *     1     | 0     | 16    = 4 exp 2
 *     1     | 1     | 64    = 4 exp 3
 *
 *
 * - TWI Bit Rate Register - TWBR
 *   - Bits 7..0 - TWI Bit Rate Register (r/w)
 *
 *
 * Calculating the TWBR value
 * ==========================
 *
 * F_SCL = (F_CPU / (16 + 2(TWBR) * (4 exp TWPS)))
 *
 * - TWBR = Value of the TWI Bit Rate Register
 *          (TWBR should be 10 or higher if the TWI operates in Master mode)
 * - TWPS = Value of the prescaler bits in the bits in the TWI Status Register
 *          (values of TWPS - two bits: 0, 1, 2, 3)
 */
int8_t twi_master_init(uint32_t f_cpu, uint32_t f_scl)
{
	uint32_t ratio = f_cpu / f_scl;
	uint32_t prescaler = 0;
	uint8_t  scaler = 0;

	if (ratio < 526) {          // max0 = 16 + 2 * 255 *  1 =   526
		prescaler = 1;
	} else if (ratio < 2056) {  // max1 = 16 + 2 * 255 *  4 =  2056
		prescaler = 4;
	} else if (ratio < 8176) {  // max2 = 16 + 2 * 255 * 16 =  8176
		prescaler = 16;
	} else if (ratio < 32656) { // max3 = 16 + 2 * 255 * 64 = 32656
		prescaler = 64;
	} else {
		// something went wrong!
		return -1;
	}

	switch (prescaler) {
		case 1:
			TWSR &= ~((1<<TWPS1)|(1<<TWPS0));
			break;
		case 4:
			TWSR &= ~(1<<TWPS1);
			TWSR |= (1<<TWPS0);
			break;
		case 16:
			TWSR |= (1<<TWPS1);
			TWSR &= ~(1<<TWPS0);
			break;
		case 64:
			TWSR |= (1<<TWPS1)|(1<<TWPS0);
			break;
		default:
			// something went wrong!
			return -1;
	}

	scaler = (uint8_t) ((ratio - 16) / (2 * prescaler));
	TWBR = scaler;

	if (scaler < 10) {
		// scaler value should not be lower than 10!
		return -2;
	}

	return 0;
}


/** Issues a start condition and sends address and transfer direction.
 *
 * @param  addr address and transfer direction of TWI device
 * @retval 0    device accessible
 * @retval 1    failed to access device
 *
 *
 * Usage
 * =====
 *
 * The parameter 'addr' gets constructed like __[A6|A5|A4|A3|A2|A1|A0|R/W]__.
 *
 * - 'Ax'  are the address bits
 * - 'R/W' the read/write bit. (0 := write; 1 := read)
 * - Use the TWI_MASTER_WRITE and TWI_MASTER_READ macro
 *
 *
 * Examples
 * ========
 *
 *	ret = twi_master_start(0x70 | TWI_MASTER_WRITE); // Address 000 for an PFC8574A: [0|1|1|1|A2|A1|A0|R/W]
 *	ret = twi_master_start(0x40 | TWI_MASTER_WRITE); // Address 000 for an PFC8574:  [0|1|0|0|A2|A1|A0|R/W]
 *
 *
 * The Registers
 * =============
 *
 * - TWI Control Register - TWCR
 *   - Bit 7 - TWINT: TWI Interrupt Flag         (r/w)
 *   - Bit 6 - TWEA:  TWI Enable Acknowledge Bit (r/w)
 *   - Bit 5 - TWSTA: TWI START Condition Bit    (r/w)
 *   - Bit 4 - TWSTO: TWI STOP  Condition Bit    (r/w)
 *   - Bit 3 - TWWC:  TWI Write Collision Flag   (r)
 *   - Bit 2 - TWEN:  TWI Enable Bit             (r/w)
 *   - Bit 1 - reserved                          (r)
 *   - Bit 0 - TWIE:  TWI Interrupt Enable       (r/w)
 */
uint8_t twi_master_start(uint8_t addr)
{
	uint8_t twst = 0x00;

	// send start condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_START) && (twst != TW_REP_START)) {
		return 1;
	}

	// send device address
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission is completed and ACK/NAK has been received
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Stauts Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ((twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK)) {
		return 1;
	}

	return 0;
}


/** Issues a repeated start and sends address and transfer direction.
 *
 * @param  addr address and transfer direction of TWI device
 * @retval 0    device accessible
 * @retval 1    failed to access device
 * @see    twi_master_start()
 */
uint8_t twi_master_rep_start(uint8_t addr)
{
	return twi_master_start(addr);
}


/** Issues a start condition and sends address and transfer direction.
 *
 * If device is busy, use ack polling to wait until device is ready.
 *
 * @param addr address and transfer direction of TWI device
 * @see   twi_master_start()
 */
void twi_master_start_wait(uint8_t addr)
{
	uint8_t twst = 0x00;

	while (1) {
		// send START condition
		TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

		// wait until transmission completed
		while (! (TWCR & (1<<TWINT)));

		// check value of TWI Status Register. Mask prescaler bits
		twst = TW_STATUS & 0xF8;
		if ((twst != TW_START) && (twst != TW_REP_START)) {
			continue;
		}

		// send device address
		TWDR = addr;
		TWCR = (1<<TWINT) | (1<<TWEN);

		// wait until transmission completed
		while (! (TWCR & (1<<TWINT)));

		// check value of TWI Status Register. Mask prescaler bits
		twst = TW_STATUS & 0xF8;
		if ((twst == TW_MT_SLA_NACK) || (twst == TW_MR_DATA_NACK)) {

			// device busy, send stop condition to terminate write operation
			TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

			// wait until stop condition is executed and bus released
			while (TWCR & (1<<TWSTO));

			continue;
		}

		break;
	}
}


/** Terminates the data transfer and releases the TWI bus. */
void twi_master_stop(void)
{
	 // send stop condition
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

	// wait until stop condition is executed and bus released
	while (TWCR & (1<<TWSTO));
}


/** Send one byte to TWI device.
 *
 * @param  data byte to be transfered
 * @retval 0    write successful
 * @retval 1    write failed
 */
uint8_t twi_master_write(uint8_t data)
{
	uint8_t twst = 0x00;

	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while (! (TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if (twst != TW_MT_DATA_ACK) {
		return 1;
	}

	return 0;
}


/** Read one byte from the TWI device, request more data from device.
 *
 * @return byte read from the TWI device
 */
uint8_t twi_master_read_ack(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	while (! (TWCR & (1<<TWINT)));

	return TWDR;
}


/** Read one byte from the TWI device, read is followed by stop condition.
 *
 * @return byte read from the TWI device
 */
uint8_t twi_master_read_nak(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN);
	while (! (TWCR & (1<<TWINT)));

	return TWDR;
}

