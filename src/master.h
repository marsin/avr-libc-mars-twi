/******************************************************************************
 Atmel - C driver - TWI master
   - TWI (I2C) master driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** TWI (I2C) master driver.
 *
 * @file      master.h
 * @see       master.c
 * @author    Peter Fleury,
 *            Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef TWI_MASTER_H
#define TWI_MASTER_H

#include <stdint.h>


/** Defines the data direction in twi_master_start() and twi_master_rep_start().
 *
 * Signals the addressed TWI IC (slave) to receive or send data.
 * The bit pattern gets added to the address by a logical OR.
 * It's the LSB of the address.
 *
 * @defgroup TWI_MASTER_RW_FLAGS
 * @see twi_master_start()
 * @see twi_master_rep_start()
 * @see twi_master_start_wait()
 *
 *
 * Example
 * =======
 *
 *	twi_master_start(0x03 + TWI_MASTER_READ);
 *	twi_master_start(0x05 | TWI_MASTER_WRITE);
 */
#define TWI_MASTER_WRITE 0x00 ///< TWI address WRITE bit flag.
                              ///< @ingroup TWI_MASTER_RW_FLAGS
#define TWI_MASTER_READ  0x01 ///< TWI address READ bit flag.
                              ///< @ingroup TWI_MASTER_RW_FLAGS


extern int8_t twi_master_init(uint32_t f_cpu, uint32_t f_scl);
extern uint8_t twi_master_start(uint8_t addr);
extern uint8_t twi_master_rep_start(uint8_t addr);
extern void twi_master_start_wait(uint8_t addr);
extern void twi_master_stop(void);
extern uint8_t twi_master_write(uint8_t data);
extern uint8_t twi_master_read_ack(void);
extern uint8_t twi_master_read_nak(void);

/** Read one byte from the TWI device.
 *
 * Implemented as macro, which calls either twi_master_read_ack() or twi_master_read_nak()
 *
 * @param  ack  1 := send ACK, request more data from device<br>
 *              0 := send NAK, read is followed by stop condition
 * @return byte read from the TWI device
 */
uint8_t twi_master_read(uint8_t ack);

/** Macro for function `uint8_t twi_master_read(uint8_t ack)`.
 *
 * @see twi_master_read()
 */
#define twi_master_read(ack) (ack) ? twi_master_read_ack() : twi_master_read_nak();


#endif // TWI_MASTER_H

