/******************************************************************************
 Atmel - C driver - twi driver - tests
   - myAVR MK3 board driver, implemented in C for Atmel microcontroller
 Copyright (c) 2016 - 2017 Martin Singer <martin.singer@web.de>
 ******************************************************************************/

/** Two Wire Interface (TWI/I2C) driver tests.
 *
 * @file      test_twi.c
 * @see       test_twi.h
 * @author    Martin Singer
 * @date      2016 - 2017
 * @copyright GNU General Public License version 3 (or in your opinion any later version)
 */


#ifndef F_CPU
  #define F_CPU 16000000ul ///< myAVR-MK3 (ATmega2560)
#endif
#ifndef F_SCL
  #define F_SCL 100000ul   ///< TWI clock in Hz (F_CPU >= 16 * F_SCL, used for TWI master)
#endif


#include <avr/interrupt.h>
#include <util/delay.h>    // requires F_CPU
#include <mars/mk3/leds.h>
#include <mars/mk3/lcd.h>
#include <mars/mk3/lcd_out.h>
#include "test.h"


#if TEST == 1
  #warning Selected TEST function is not tested yet!
/** TEST Function: TWI master - transmitter - configured for a port extension IC.
 *
 * This is a special case of master_transmitter_reset_mode().
 *
 *
 * Function
 * --------
 *
 * In this function the master initiates for every data byte to send a start procedure.
 *
 * 1. The master initiates the start procedure (addressing the slave)
 * 2. After the ACK signal, the master sends one data byte.
 * 3. The master quits the connection.
 * 4. The master waits a delay time.
 * 5. ... goto 1. ...
 *
 *
 * Configuration
 * -------------
 *
 * The TWI of the MC is initialized as master.
 */
void test_twi_master_transmitter_port_extension(void)
{
	uint8_t addr = 0x20; // [0|0|1|0|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	mk3_led_init();
	twi_master_init(F_CPU, F_SCL);
	mk3_led_set_output(0xFF);

	while (1) {
		for (char c = 'A'; c <= 'Z'; ++c) {               // [255..1]
			_delay_ms(250);
			ret = twi_master_start(addr | TWI_MASTER_WRITE);
			if (0 != ret) {
				twi_master_stop();
				mk3_led_set_output(addr);         // Signal Fail
			} else {
				data = (uint8_t) c;
				ret = twi_master_write(data);
				twi_master_stop();
				if (0 == ret) {
					mk3_led_set_output(0x01); // Signal OK
				} else {
					mk3_led_set_output(0x02); // Signal Fail
					break;
				}
			}
		}
	}

	return;
}


#elif TEST == 2
  #warning Selected TEST function is not tested yet!
/** TEST Function: TWI master - transmitter - configured for simple data sending.
 *
 * Function
 * --------
 *
 * In this function the master initiates a start procedure once
 * for a whole data byte series.
 *
 * 1. The master initiates the start procedure (addressing the slave).
 * 2. The master sends data byte after data byte of the data series ...
 * 3. The master quits the connection.
 * 4. The master waits a delay time.
 * 5. ... goto 1. ...
 *
 *
 * Configuration
 * -------------
 *
 * The TWI of the MC is initialized as master.
 */
void test_twi_master_transmitter_reset_mode(void)
{
	uint8_t addr = 0x20; // [0|0|1|0|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	mk3_led_init();
	twi_master_init(F_CPU, F_SCL);
	mk3_led_set_output(0xFF);

	while (1) {
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
			mk3_led_set_output(addr);                 // Signal Fail
		} else {
			for (char c = 'A'; c <= 'Z'; ++c) {       // [A..Z]
				data = (uint8_t) c;
				ret = twi_master_write(data);
				if (0 == ret) {
					mk3_led_set_output(0x01); // Signal OK
				} else {
					mk3_led_set_output(0x02); // Signal Fail
					break;
				}
			}
			twi_master_stop();
			_delay_ms(2500);
		}
	}

	return;
}


#elif TEST == 3
  #error Selected TEST function is not implemented yet!
/** TEST Function: TWI master - transmitter - configured for addressed data sending.
 *
 * For use of a EEPROM memory IC.
 */
void test_twi_master_transmitter_address_mode(void)
{
	// ...
	return;
}


#elif TEST == 4
  #error Selected TEST function is not implemented yet!


#elif TEST == 5
  #error Selected TEST function is not implemented yet!


#elif TEST == 6
  #error Selected TEST function is not implemented yet!


#elif TEST == 7
  #error Selected TEST function is not implemented yet!


#elif TEST == 8
  #error Selected TEST function is not implemented yet!


#elif TEST == 9
  #error Selected TEST function is not implemented yet!


#elif TEST == 10
/** TEST Function: TWI slave - receiver - configured like a port extension IC.
 *
 * ## Function
 *
 * This function enables the TWI slave mode.
 * Every byte of a data transfer becomes written in buffer.data[0]
 * It polls continuously the first slave buffer data byte
 * and copies it to the LED register of the board.
 *
 * It is woks like a port extension IC (without addressing procedure).
 *
 *
 * ## Configuration
 *
 * - TWI slave
 * - TWI address is 0x70.
 * - Max register is 0 (means a size of one register byte - register 0).
 * - TWI_SLAVE_MODE_RESET sets the non-addressing mode.
 */
void test_twi_slave_receiver_port_extension(void)
{
	mk3_led_set_output(0xFF);

	mk3_led_init();
	twi_slave_init(0x30, 0, TWI_SLAVE_MODE_RESET);

	sei();

	while (1) {
		mk3_led_set_output(twi_slave_buffer.data[0]);
	}
}


#elif TEST == 11
/** TEST Function: TWI slave - receiver - configured for simple data receiving.
 *
 * ## Function
 *
 * - Prints the buffer bytes [0..159], interpreted as ASCII characters on the LCD.
 *   The other, also declared bytes [160..255] become not shown.
 *   (it's on the master to send a useful amount of data bytes)
 * - Transfered data bytes become stored in the buffer array.
 * - With every new transmission start
 *   the buffer address counter becomes reseted to position buffer.data[0].
 * - Is a transmission longer than the buffer size,
 *   the buffer address counter wraps around to buffer.data[0]
 *   (overwrites already sent data bytes).
 *
 *
 * ## Configuration
 *
 * - TWI slave
 * - TWI address is 0x70
 * - The buffer size is set to 256 (max = size - 1)
 * - TWI_SLAVE_MODE_RESET sets the non-addressing mode.
 */
void test_twi_slave_receiver_reset_mode(void)
{
	uint8_t size = 168; // LCD size: 21 signs per line, 8 lines

	mk3_lcd_init();
	twi_slave_init(0x30, 255, TWI_SLAVE_MODE_RESET);

	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	sei();

	while (1) {
//		mk3_lcd_clear();
		mk3_lcd_set_pos(0, 0);
		for (uint8_t i = 0; i < size; ++i) {
			if (i % 21 == 0) {
				if (i / 21 < 8) {
					mk3_lcd_set_pos(0, 8 * (i / 21)); // next line
				} else {
					mk3_lcd_set_pos(0, 0); // first line
				}
			}
			mk3_lcd_put_char5x7(twi_slave_buffer.data[i]);
		}
	}
	return;
}


#elif TEST == 12
/** TEST Function: TWI slave - receiver - configured for simple data receiving.
 *
 * ## Function
 *
 * like test_twi_slave_receiver_reset_mode(),
 * but the buffer position is set by the first data byte.
 *
 * ## Configuration
 *
 * - TWI slave
 * - TWI address is 0x70
 * - The buffer size is set to 256 (max = size - 1)
 * - TWI_SLAVE_MODE_ADDRESS sets the non-addressing mode.
 */
void test_twi_slave_receiver_address_mode(void)
{
	uint8_t size = 168; // LCD size: 21 signs per line, 8 lines

	mk3_lcd_init();
	twi_slave_init(0x30, 255, TWI_SLAVE_MODE_ADDRESS);

	mk3_lcd_clear();
	mk3_lcd_set_pos(0, 0);
	mk3_lcd_light(1);

	sei();

	while (1) {
		mk3_lcd_set_pos(0, 0);
		for (uint8_t i = 0; i < size; ++i) {
			if (i % 21 == 0) {
				if (i / 21 < 8) {
					mk3_lcd_set_pos(0, 8 * (i / 21)); // next line
				} else {
					mk3_lcd_set_pos(0, 0); // first line
				}
			}
//			mk3_lcd_put_char5x7(twi_slave_buffer.data[i]);
			mk3_lcd_putchar(twi_slave_buffer.data[i], NULL);
		}
	}
	return;
}


#elif TEST == 254
/** TEST Function: TWI (Two Wire Interface) master - blinken light.
 *
 * IC
 * --
 *
 * This function is tested with an PCF8574AP IC.
 *
 * - Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
 * - The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
 * - The maximal operation frequency is 100kHz              (_Page 16_)
 *
 *
 * Circuit
 * -------
 *
 *	                         DIP16
 *	                     ---------
 *	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
 *	GND -------------A1-|02     15|-SDA--------'------ SDA
 *	GND -------------A2-|03     14|-SCL--------.------ SCL
 *	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
 *	                -P1-|05  C  12|-P7-
 *	                -P2-|06     11|-P6-
 *	                -P3-|07     10|-P5-
 *	GND ------------Vss-|08     09|-P4-
 *	                     ---------
 *	                     PCF8574AP
 *
 *
 * Function
 * --------
 *
 * The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
 * When the connections works, the MC sends data bytes
 * which becomes written on the port output of the port extension IC.
 * Is a data bit low, the drain get connected to GND and the LED lights up.
 */
void example_twi_master_blink(void)
{
	uint8_t addr = 0x70; // PFC8574AP: [0|1|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t data = 0x00;
	uint8_t ret  = 0x00;

	mk3_led_init();
	twi_master_init(F_CPU, F_SCL);
	mk3_led_set_output(0xFF);

	while (1) {
		_delay_ms(250);

		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
			mk3_led_set_output(addr);         // Signal Fail
		} else {
			ret = twi_master_write(data);
			twi_master_stop();
			if (0 == ret) {
				mk3_led_set_output(0x01); // Signal OK
			} else {
				mk3_led_set_output(0x02); // Signal Fail
			}
		}

		if (0x00 == data) {
			data = 0xFF; // LEDs off
		} else {
			data = 0x00; // LEDs on
		}
	}

	return;
}


#elif TEST == 255
/** TEST Function: TWI (Two Wire Interface) master - button and light.
 *
 * IC
 * --
 *
 * This function is tested with an PCF8574AP IC.
 *
 * - Datasheet: <http://www.nxp.com/documents/data_sheet/PCF8574.pdf>
 * - The address byte looks like __[0|1|1|1|A2|A1|A0|R/W]__ (_Page  9_)
 * - The maximal operation frequency is 100kHz              (_Page 16_)
 * - The I/Os should be HIGH before being used as inputs    (_Page 12_)
 * - This IC has a 100uA current driver.
 *   Because of this, the button SW0 requires no pre resistor or pull-up resistor.
 *   <https://www.mikrocontroller.net/articles/Port-Expander_PCF8574>
 *
 *
 * Circuit
 * -------
 *
 *	                         DIP16
 *	                     ---------
 *	GND -------------A0-|01  #  16|-Vdd-- +5V  .-Rp1-- +5V
 *	GND -------------A1-|02     15|-SDA--------'------ SDA
 *	GND -------------A2-|03     14|-SCL--------.------ SCL
 *	+5V --LED>|--Rv--P0-|04  I  13|-/INT-      '-Rp2-- +5V
 *	                -P1-|05  C  12|-P7-------SW0------ GND
 *	                -P2-|06     11|-P6-
 *	                -P3-|07     10|-P5-
 *	GND ------------Vss-|08     09|-P4-
 *	                     ---------
 *	                     PCF8574AP
 *
 *
 * Function
 * --------
 *
 * The MC (master) initiates a Connection with the port extension PCF8674AP IC (slave).
 * When the connections works, the MC sends 0xFF for init the PORTs first.
 * The I/Os should be HIGH before being used as inputs.
 * The TWI-IC gets read, the state of P7 (button SW0) checked and P0 (LED),
 * depending on the state of P7, written.
 */
void example_twi_master_button(void)
{
	uint8_t addr = 0x70; // PFC8574AP: [0|1|1|1|A2|A1|A0|R/W] --> 0111000x
	uint8_t ret  = 0x00;
	uint8_t data = 0x00;

	mk3_led_init();
	twi_master_init(F_CPU, F_SCL);
	mk3_led_set_output(0xFF);

	// Init TWI IC PORTs
	ret = twi_master_start(addr | TWI_MASTER_WRITE);
	if (0 != ret) {
		twi_master_stop();
		mk3_led_set_output(addr);  // show signal for fail
	} else {
		ret = twi_master_write(0xFF); // set all PORTs HIGH (to VDD)
		twi_master_stop();
	}

	// Test Loop
	while (1) {
		// read button
		ret = twi_master_start(addr | TWI_MASTER_READ);
		if (0 != ret) {
			twi_master_stop();
			mk3_led_set_output(addr);          // show signal for fail
		} else {
			data = twi_master_read_nak();         // read and stop twi
		}
		// set led
		ret = twi_master_start(addr | TWI_MASTER_WRITE);
		if (0 != ret) {
			twi_master_stop();
			mk3_led_set_output(addr);          // show signal for fail
		} else {
			if (data & 0x80) {             // if  PIN7 is HIGH (Button released)
				ret = twi_master_write(0xFE); // set PIN0 to LOW  (LED on)
			} else {
				ret = twi_master_write(0xFF); // set PIN0 to HIGH (LED off)
			}
			twi_master_stop();
		}
	}
	return;
}
#endif // TEST

